package com.reem.univer_pro.qualification.repository;

import com.reem.univer_pro.qualification.entityDTO.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface QualificationRepository extends JpaRepository<Qualification, UUID> {

    Qualification findByQualificationId(UUID id);

    void deleteByQualificationId(UUID id);
}
