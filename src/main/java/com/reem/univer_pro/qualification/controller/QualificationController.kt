package com.reem.univer_pro.qualification.controller

import com.reem.univer_pro.qualification.entityDTO.QualificationDTO
import com.reem.univer_pro.qualification.entityDTO.QualificationUpdateDTO
import com.reem.univer_pro.qualification.service.QualificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/qualify")
class QualificationController {

    @Autowired
    lateinit var service: QualificationService

    @PostMapping("/add/{userId}")
    fun addQualification(@Valid @RequestBody qualificationDTO: QualificationDTO, @PathVariable userId: UUID): ResponseEntity<QualificationDTO> {
        return ResponseEntity.ok(service.addQualification(qualificationDTO, userId))
    }


    @PutMapping("/update/{id}")
    fun updateQualification(@PathVariable id: UUID, @RequestBody qDTO: QualificationUpdateDTO): ResponseEntity<QualificationDTO> {
           return ResponseEntity.ok(service.updateQualification(qDTO, id))
    }


    @GetMapping("/{id}")
    fun getQualification(@PathVariable id: UUID): ResponseEntity<QualificationDTO> {
        return ResponseEntity.ok(service.getQualification(id))
    }

    @DeleteMapping("delete/{id}")
    fun deleteQualification(@PathVariable id: UUID): ResponseEntity<String> {
        service.deleteQualification(id)
        return ResponseEntity.ok("Done")
    }

}