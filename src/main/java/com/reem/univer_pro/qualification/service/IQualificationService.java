package com.reem.univer_pro.qualification.service;

import com.reem.univer_pro.qualification.entityDTO.QualificationDTO;
import com.reem.univer_pro.qualification.entityDTO.QualificationUpdateDTO;

import java.util.UUID;


public interface IQualificationService {

    QualificationDTO addQualification(QualificationDTO qDTO, UUID userID);

    QualificationDTO updateQualification(QualificationUpdateDTO qUpdateDTO, UUID qId);

    QualificationDTO getQualification(UUID id);

    void deleteQualification(UUID id);
}
