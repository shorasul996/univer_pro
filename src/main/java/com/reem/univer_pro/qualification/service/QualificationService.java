package com.reem.univer_pro.qualification.service;

import com.reem.univer_pro.excaption.*;
import com.reem.univer_pro.qualification.entityDTO.Qualification;
import com.reem.univer_pro.qualification.entityDTO.QualificationDTO;
import com.reem.univer_pro.qualification.entityDTO.QualificationUpdateDTO;
import com.reem.univer_pro.qualification.repository.QualificationRepository;
import com.reem.univer_pro.user.entityDTO.User;
import com.reem.univer_pro.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class QualificationService implements IQualificationService {

    private final QualificationRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public QualificationService(QualificationRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }


    @Override
    public QualificationDTO addQualification(QualificationDTO qDTO, UUID userId) {

        User user = userRepository.findUserByUserId(userId);

        if (user != null) {

            Date now = new Date();
            List<Qualification> qualificationList = user.getQualificationList();
            Qualification qualification = new Qualification();

            qualification.setInstitutionName(qDTO.getInstitutionName());
            qualification.setUserId(userId);
            qualification.setDegree(qDTO.getDegree());
            qualification.setFaculty(qDTO.getFaculty());
            qualification.setFinished(qDTO.isFinished());
            qualification.setTimeCreated(now);
            qualification.setTimeModified(now);
            repository.save(qualification);

            qualificationList.add(qualification);
            user.setQualificationList(qualificationList);

            qDTO.setQualificationId(qualification.getQualificationId());
            qDTO.setUserId(qualification.getUserId());
            return qDTO;
        }
        return null;
    }

    @Override
    public QualificationDTO updateQualification(QualificationUpdateDTO qUpdateDTO, UUID qId) {

        Qualification qualification = repository.findByQualificationId(qId);
        if (Objects.isNull(qualification)) {
            throw new EntityNotFoundException(Qualification.class, qId);
        }
        qualification.setInstitutionName(qUpdateDTO.getInstitutionName());
        qualification.setDegree(qUpdateDTO.getDegree());
        qualification.setFaculty(qUpdateDTO.getFaculty());
        qualification.setFinished(qUpdateDTO.isFinished());
        qualification.setTimeModified(new Date());

        repository.save(qualification);
        return new QualificationDTO().build(qualification);
    }

    @Override
    public QualificationDTO getQualification(UUID id) {
        Qualification qualification = repository.findByQualificationId(id);
        if (Objects.isNull(qualification)) {
            throw new EntityNotFoundException(Qualification.class, id);
        }
        return new QualificationDTO().build(qualification);

    }

    @Override
    public void deleteQualification(UUID id) {
        repository.deleteByQualificationId(id);
    }

}
