package com.reem.univer_pro.qualification.entityDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class QualificationDTO {

    private UUID qualificationId;
    private UUID userId;

    @JsonProperty("degree")
    private String degree;

    @JsonProperty("institution_name")
    private String institutionName;

    @JsonProperty("faculty")
    private String faculty;

    @JsonProperty("isFinished")
    private boolean isFinished;



    public QualificationDTO build(Qualification qualification){
        this.qualificationId = qualification.getQualificationId();
        this.userId = qualification.getUserId();
        this.degree = qualification.getDegree();
        this.institutionName = qualification.getInstitutionName();
        this.faculty = qualification.getFaculty();
        this.isFinished = qualification.isFinished();

        return this;
    }
}
