package com.reem.univer_pro.qualification.entityDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "qualification")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Qualification {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "qualification_id")
    private UUID qualificationId;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "degree")
    @Size(max = 100, message = "degree can not be more than 100 characters.")
    private String degree;

    @Column(name = "institution_name")
    @Size(max = 100, message = "institution_name can not be more than 100 characters.")
    private String institutionName;

    @Column(name = "faculty")
    @Size(max = 100, message = "faculty can not be more than 100 characters.")
    private String faculty;

    @Column(name = "is_finished")
    private boolean isFinished;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_created")
    private Date timeCreated = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_Modified")
    private Date timeModified = new Date();

}
