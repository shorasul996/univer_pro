package com.reem.univer_pro.qualification.entityDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QualificationUpdateDTO {

    @JsonProperty("degree")
    private String degree;

    @JsonProperty("institution_name")
    private String institutionName;

    @JsonProperty("faculty")
    private String faculty;

    @JsonProperty("isFinished")
    private boolean isFinished;


}
