package com.reem.univer_pro.family.controller

import com.reem.univer_pro.family.entityDTO.FamilyDTO
import com.reem.univer_pro.family.entityDTO.FamilyUpdateDTO
import com.reem.univer_pro.family.service.FamilyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/family")
class FamilyController {

    @Autowired
    lateinit var service: FamilyService

    @PostMapping("/add/{userId}")
    fun addFamily(@Valid @RequestBody familyDTO: FamilyDTO, @PathVariable userId: UUID): ResponseEntity<FamilyDTO> {
        return ResponseEntity.ok(service.addFamily(familyDTO, userId))
    }


    @PutMapping("/update/{id}")
    fun updateFamily(@PathVariable id: UUID, @RequestBody familyUpdateDTO: FamilyUpdateDTO): ResponseEntity<FamilyDTO> {
           return ResponseEntity.ok(service.updateFamily(familyUpdateDTO, id))
    }


    @GetMapping("/{id}")
    fun getFamily(@PathVariable id: UUID): ResponseEntity<FamilyDTO> {
        return ResponseEntity.ok(service.getFamily(id))
    }

    @DeleteMapping("delete/{id}")
    fun deleteFamily(@PathVariable id: UUID): ResponseEntity<String> {
        service.deleteFamily(id)
       return ResponseEntity.ok("Done")
    }

}