package com.reem.univer_pro.family.service;

import com.reem.univer_pro.family.entityDTO.Family;
import com.reem.univer_pro.family.entityDTO.FamilyDTO;
import com.reem.univer_pro.family.entityDTO.FamilyUpdateDTO;
import com.reem.univer_pro.family.repository.FamilyRepository;
import com.reem.univer_pro.user.entityDTO.User;
import com.reem.univer_pro.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.reem.univer_pro.excaption.*;


import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class FamilyService implements iFamilyService {

    private final FamilyRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public FamilyService(FamilyRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public FamilyDTO addFamily(FamilyDTO familyDTO, UUID userId) {

        User user = userRepository.findUserByUserId(userId);

        if (user != null) {
            Date now = new Date();
            List<Family> familyList = user.getFamilyList();
            Family family = new Family();
            family.setUserId(userId);
            family.setFirstName(familyDTO.getFirstName());
            family.setLastName(familyDTO.getLastName());
            family.setAge(familyDTO.getAge());
            family.setSex(familyDTO.getSex());
            family.setTimeCreated(now);
            family.setTimeModified(now);
            repository.save(family);

            familyList.add(family);
            user.setFamilyList(familyList);

            familyDTO.setFamilyId(family.getFamilyId());
            familyDTO.setUserId(family.getUserId());
            return familyDTO;
        }

        return null;
    }

    @Override
    public FamilyDTO updateFamily(FamilyUpdateDTO familyUpdateDTO, UUID id) {

        Family family = repository.findByFamilyId(id);
        if (Objects.isNull(family)) {
            throw new EntityNotFoundException(Family.class, id);
        }
        family.setFirstName(familyUpdateDTO.getFirstName());
        family.setLastName(familyUpdateDTO.getLastName());
        family.setSex(familyUpdateDTO.getSex());
        family.setAge(familyUpdateDTO.getAge());
        family.setTimeModified(new Date());

        repository.save(family);
        return new FamilyDTO().build(family);
    }

    @Override
    public FamilyDTO getFamily(UUID id) {
        Family family = repository.findByFamilyId(id);
        if (Objects.isNull(family)) {
            throw new EntityNotFoundException(Family.class, id);
        }
        return new FamilyDTO().build(family);

    }

    @Override
    public void deleteFamily(UUID id) {
        repository.deleteById(id);
    }

}
