package com.reem.univer_pro.family.service;

import com.reem.univer_pro.family.entityDTO.FamilyDTO;
import com.reem.univer_pro.family.entityDTO.FamilyUpdateDTO;

import java.util.UUID;


public interface iFamilyService {

    FamilyDTO addFamily(FamilyDTO familyDTO, UUID userId);

    FamilyDTO updateFamily(FamilyUpdateDTO familyUpdateDTO, UUID id);

    FamilyDTO getFamily(UUID id);

    void deleteFamily(UUID id);
}
