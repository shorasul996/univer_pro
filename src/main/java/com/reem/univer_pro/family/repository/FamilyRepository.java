package com.reem.univer_pro.family.repository;

import com.reem.univer_pro.family.entityDTO.Family;
import com.reem.univer_pro.family.entityDTO.FamilyDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FamilyRepository extends JpaRepository<Family, UUID> {

    Family findByFamilyId(UUID id);

}
