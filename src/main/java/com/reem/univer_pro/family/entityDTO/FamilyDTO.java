package com.reem.univer_pro.family.entityDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class FamilyDTO {

    private UUID familyId;

    private UUID userId;

    @NotEmpty(message = "first_name can not be empty")
    @JsonProperty("first_name")
    private String firstName;

    @NotEmpty(message = "last_name can not be empty")
    @JsonProperty("last_name")
    private String lastName;


    @NotEmpty(message = "f_sex can not be empty.")
    @JsonProperty("sex")
    private String sex;

    @NotEmpty(message = "user_age can not be empty.")
    @JsonProperty("age")
    private String age;


    public FamilyDTO build(Family family) {
        this.familyId = family.getFamilyId();
        this.userId = family.getUserId();
        this.firstName = family.getFirstName();
        this.lastName = family.getLastName();
        this.sex = family.getSex();
        this.age = family.getAge();

        return this;
    }
}
