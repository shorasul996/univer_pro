package com.reem.univer_pro.family.entityDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "family_member")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Family {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "family_id")
    private UUID familyId;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "first_name")
    @NotEmpty(message = "first_name can not be empty.")
    @Size(max = 100, message = "first_name can not be more than 100 characters.")
    private String firstName;

    @Column(name = "last_name")
    @NotEmpty(message = "last_name can not be empty.")
    @Size(max = 100, message = "last_name can not be more than 100 characters.")
    private String lastName;

    @Column(name = "sex")
    @NotEmpty(message = "sex can not be empty.")
    @Size(max = 100, message = "sex can not be more than 100 characters.")
    private String sex;

    @Column(name = "age")
    @NotEmpty(message = "age can not be empty.")
    @Size(max = 100, message = "age can not be more than 100 characters.")
    private String age;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_created")
    private Date timeCreated = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_modified")
    private Date timeModified = new Date();

}
