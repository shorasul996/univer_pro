package com.reem.univer_pro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.reem.univer_pro.user",
        "com.reem.univer_pro.certification",
        "com.reem.univer_pro.qualification",
        "com.reem.univer_pro.career",
        "com.reem.univer_pro.family",
})
@EntityScan
@EnableJpaAuditing
public class UniverProApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniverProApplication.class, args);
    }

}
