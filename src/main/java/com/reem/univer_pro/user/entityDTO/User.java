package com.reem.univer_pro.user.entityDTO;

import com.reem.univer_pro.career.entityDTO.Career;
import com.reem.univer_pro.certification.entityDTO.Certification;
import com.reem.univer_pro.family.entityDTO.Family;
import com.reem.univer_pro.qualification.entityDTO.Qualification;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "first_name")
    @NotEmpty(message = "first_name can not be empty.")
    @Size(max = 100, message = "first_name can not be more than 100 characters.")
    private String userFirstName;

    @Column(name = "last_name")
    @NotEmpty(message = "last_name can not be empty.")
    @Size(max = 100, message = "last_name can not be more than 100 characters.")
    private String userLastName;

    @Email
    @Column(name = "email", unique = true)
    @NotEmpty(message = "email can not be empty.")
    @Size(max = 100, message = "email can not be more than 100 characters.")
    private String email;

    @Column(name = "password")
    @NotEmpty(message = "password can not be empty.")
    @Size(max = 100, message = "password can not be more than 100 characters.")
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_created")
    private Date timeCreated = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_modified")
    private Date timeModified = new Date();


    @OneToMany
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private List<Certification> certificationList;

    @OneToMany
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private List<Qualification> qualificationList;

    @OneToMany
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private List<Career> careerList;

    @OneToMany
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private List<Family> familyList;

}
