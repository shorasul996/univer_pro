package com.reem.univer_pro.user.entityDTO;

import com.fasterxml.jackson.annotation.*;
import com.reem.univer_pro.certification.entityDTO.Certification;
import lombok.*;
import org.hibernate.validator.constraints.*;

import java.util.List;

@Getter
@Setter
public class UserUpdateDTO {

    @NotEmpty(message = "first_name can not be empty")
    @JsonProperty("first_name")
    private String firstName;

    @NotEmpty(message = "last_name can not be empty")
    @JsonProperty("last_name")
    private String lastName;

    @NotEmpty(message = "email can not be empty")
    @JsonProperty("email")
    private String email;

    @JsonProperty("certificationList")
    private List<Certification> certificationList;

}
