package com.reem.univer_pro.user.entityDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.reem.univer_pro.certification.entityDTO.Certification;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class UserDTO {

    private UUID userId;

    @NotEmpty(message = "first_name can not be empty")
    @JsonProperty("first_name")
    private String firstName;

    @NotEmpty(message = "last_name can not be empty")
    @JsonProperty("last_name")
    private String lastName;

    @Email
    @JsonProperty("email")
    private String email;

    @NotEmpty(message = "password can not be empty")
    @Size(min = 4, message = "password must be at least 6 character")
    @JsonProperty("password")
    private String password;

    @JsonProperty("certificationList")
    private List<Certification> certificationList;


    public UserDTO build(User user){
        this.userId = user.getUserId();
        this.firstName = user.getUserFirstName();
        this.lastName = user.getUserLastName();
        this.email = user.getEmail();
        this.certificationList = user.getCertificationList();

        return this;
    }
}
