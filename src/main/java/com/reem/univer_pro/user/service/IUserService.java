package com.reem.univer_pro.user.service;

import com.reem.univer_pro.user.entityDTO.UserDTO;
import com.reem.univer_pro.user.entityDTO.UserUpdateDTO;

import java.util.UUID;


public interface IUserService {

    UserDTO addUser(UserDTO user);

    UserDTO updateUser(UserUpdateDTO user, UUID userId);

    UserDTO getUser(UUID id);

    void deleteUser(UUID id);
}
