package com.reem.univer_pro.user.service;

import com.reem.univer_pro.excaption.*;
import com.reem.univer_pro.user.entityDTO.User;
import com.reem.univer_pro.user.entityDTO.UserDTO;
import com.reem.univer_pro.user.entityDTO.UserUpdateDTO;
import com.reem.univer_pro.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/*
* Test for commit and push v0.1
*
*
* */

@Service
public class UserService implements IUserService{

    private final UserRepository repository;
    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDTO addUser(UserDTO userDTO) {

        Date now = new Date();
        User user = new User();
        user.setUserFirstName(userDTO.getFirstName());
        user.setUserLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setCertificationList(userDTO.getCertificationList());

        user.setTimeCreated(now);
        user.setTimeModified(now);
        repository.save(user);

        userDTO.setUserId(user.getUserId());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    @Override
    public UserDTO updateUser(UserUpdateDTO userUpdateDTO, UUID userId) {

        User user = repository.findUserByUserId(userId);
        if(Objects.isNull(user)){
            throw new EntityNotFoundException(User.class, userId);
        }
        user.setUserFirstName(userUpdateDTO.getFirstName());
        user.setUserLastName(userUpdateDTO.getLastName());
        user.setEmail(userUpdateDTO.getEmail());
        user.setTimeModified(new Date());

        repository.save(user);
        return new UserDTO().build(user);


    }


    @Override
    public UserDTO getUser(UUID id) {

        User user = repository.findUserByUserId(id);
        if(Objects.isNull(user)) {
            throw new EntityNotFoundException(User.class, id);
        }
        return new UserDTO().build(user);
    }

    @Override
    public void deleteUser(UUID id) {
        repository.deleteById(id);
    }
}
