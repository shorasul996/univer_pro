package com.reem.univer_pro.user.controller

import com.reem.univer_pro.user.entityDTO.UserDTO
import com.reem.univer_pro.user.entityDTO.UserUpdateDTO
import com.reem.univer_pro.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/user")
class UserControllerKotlin {

    @Autowired
    lateinit var service: UserService

    @PostMapping("/add")
    fun addUser(@Valid @RequestBody user: UserDTO): ResponseEntity<UserDTO> {
        return ResponseEntity.ok(service.addUser(user))
    }


    @PutMapping("/update/{userId}")
    fun updateUser(@PathVariable(name = "userId") userId: UUID, @RequestBody user: UserUpdateDTO): ResponseEntity<UserDTO> {
           return ResponseEntity.ok(service.updateUser(user, userId))
    }


    @GetMapping("/{userId}")
    fun getUser(@PathVariable(name = "userId") userId: UUID): ResponseEntity<UserDTO> {
        return ResponseEntity.ok(service.getUser(userId))
    }

    @DeleteMapping("delete/{id}")
    fun deleteUser(@PathVariable id: UUID): ResponseEntity<String> {
        service.deleteUser(id)
       return ResponseEntity.ok("Done")
    }

}