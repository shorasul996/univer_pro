//package com.reem.univer_pro.user.controller;
//
//import com.reem.univer_pro.user.entity.User;
//import com.reem.univer_pro.user.service.UserService;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//import java.util.Optional;
//import java.util.UUID;
//
//@RestController
//@RequestMapping("/user")
//public class UserController {
//
//    private final UserService service;
//
//    public UserController(UserService service) {
//        this.service = service;
//    }
//
//
//    @PostMapping("/add")
//    String createUser(@RequestBody User user){
//        service.saveUser(user);
//        return "User successfully created";
//    }
//
//    @GetMapping( "/all")
//    List<User> findAll(){
//        return service.getAll();
//    }
//
//    @GetMapping("/{userId}")
//    User findUserByName(@PathVariable(name = "userId") UUID userId){
//        return service.getUser(userId);
//    }
//
//    @PutMapping("/update")
//    String updateUserInfo(@RequestBody User user){
//        service.updateUser(user);
//        return "User info successfully updated";
//    }
//
//    @DeleteMapping( "/delete")
//    String deleteUser(@RequestBody UUID userId){
//        service.deleteUser(userId);
//        return "User successfully deleted";
//    }
//}
