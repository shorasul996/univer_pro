package com.reem.univer_pro.user.repository;

import com.reem.univer_pro.user.entityDTO.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    User findUserByUserId(UUID id);

    void deleteByUserId(UUID id);

}
