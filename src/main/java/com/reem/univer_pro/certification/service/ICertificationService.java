package com.reem.univer_pro.certification.service;

import com.reem.univer_pro.certification.entityDTO.CertificationDTO;
import com.reem.univer_pro.certification.entityDTO.CertificationUpdateDTO;

import java.util.UUID;


public interface ICertificationService {

    CertificationDTO addCer(CertificationDTO cerDTO, UUID userId);

    CertificationDTO updateCer(CertificationUpdateDTO cerUpdateDTO, UUID id);

    CertificationDTO getCertification(UUID id);

    void deleteCer(UUID id);
}
