package com.reem.univer_pro.certification.service;

import com.reem.univer_pro.excaption.*;
import com.reem.univer_pro.certification.entityDTO.Certification;
import com.reem.univer_pro.certification.entityDTO.CertificationDTO;
import com.reem.univer_pro.certification.entityDTO.CertificationUpdateDTO;
import com.reem.univer_pro.certification.repository.CertificationRepository;
import com.reem.univer_pro.user.entityDTO.User;
import com.reem.univer_pro.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class CertificationService implements ICertificationService {

    private final CertificationRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public CertificationService(CertificationRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public CertificationDTO addCer(CertificationDTO cerDTO, UUID userId) {

        if (userRepository.getOne(userId) != null) {
            User user = userRepository.findUserByUserId(userId);
            List<Certification> certificationList = user.getCertificationList();

            Date now = new Date();
            Certification certification = new Certification();
            certification.setCerName(cerDTO.getCerName());
            certification.setCerType(cerDTO.getCerType());
            certification.setUserId(userId);
            certification.setTimeCreated(now);
            certification.setTimeModified(now);
            repository.save(certification);

            certificationList.add(certification);
            user.setCertificationList(certificationList);

            cerDTO.setCerId(certification.getCerId());
            cerDTO.setUserId(certification.getUserId());


            return cerDTO;
        }
        return null;
    }

    @Override
    public CertificationDTO updateCer(CertificationUpdateDTO cerUpDTO, UUID id) {

        Certification certification = repository.findByCerId(id);
        if (Objects.isNull(certification)) {
            throw new EntityNotFoundException(Certification.class, id);
        }
        certification.setCerName(cerUpDTO.getCerName());
        certification.setCerType(cerUpDTO.getCerType());
        certification.setTimeModified(new Date());

        repository.save(certification);
        return new CertificationDTO().build(certification);


    }


    @Override
    public CertificationDTO getCertification(UUID id) {

        Certification certification = repository.findByCerId(id);
        if (Objects.isNull(certification)) {
            throw new EntityNotFoundException(Certification.class, id);
        }
        return new CertificationDTO().build(certification);
    }

    @Override
    public void deleteCer(UUID id) {
        repository.deleteById(id);
    }
}
