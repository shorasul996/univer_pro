package com.reem.univer_pro.certification.entityDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CertificationDTO {

    private UUID cerId;
    private UUID userId;

    @NotEmpty(message = "cer_name can not be empty")
    @JsonProperty("cer_name")
    private String cerName;

    @NotEmpty(message = "cer_type can not be empty")
    @JsonProperty("cer_type")
    private String cerType;




    public CertificationDTO build(Certification certification){
        this.cerId = certification.getCerId();
        this.userId = certification.getUserId();
        this.cerName = certification.getCerName();
        this.cerType = certification.getCerType();

        return this;
    }
}
