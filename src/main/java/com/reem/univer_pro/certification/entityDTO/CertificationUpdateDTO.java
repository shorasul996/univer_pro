package com.reem.univer_pro.certification.entityDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class CertificationUpdateDTO {

    @NotEmpty(message = "cer_name can not be empty")
    @JsonProperty("cer_name")
    private String cerName;

    @NotEmpty(message = "cer_type can not be empty")
    @JsonProperty("cer_type")
    private String cerType;


}
