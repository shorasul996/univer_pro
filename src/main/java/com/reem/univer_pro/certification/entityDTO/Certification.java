package com.reem.univer_pro.certification.entityDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "certification")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Certification {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "certification_id")
    private UUID cerId;

    @Column(name = "cer_name")
    @NotEmpty(message = "cer_name can not be empty.")
    @Size(max = 100, message = "cer_name can not be more than 100 characters.")
    private String cerName;

    @Column(name = "cer_type")
    @NotEmpty(message = "cer_type can not be empty.")
    @Size(max = 100, message = "cer_type can not be more than 100 characters.")
    private String cerType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_created")
    private Date timeCreated = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_modified")
    private Date timeModified = new Date();

    @Column(name = "user_id")
    private UUID userId;

}
