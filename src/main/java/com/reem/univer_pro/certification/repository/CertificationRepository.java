package com.reem.univer_pro.certification.repository;

import com.reem.univer_pro.certification.entityDTO.Certification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CertificationRepository extends JpaRepository<Certification, UUID> {

    Certification findByCerId(UUID id);

    void deleteByCerId(UUID id);
}
