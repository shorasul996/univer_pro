package com.reem.univer_pro.certification.controller

import com.reem.univer_pro.certification.entityDTO.CertificationDTO
import com.reem.univer_pro.certification.entityDTO.CertificationUpdateDTO
import com.reem.univer_pro.certification.service.CertificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/cer")
class CertificationController {

    @Autowired
    lateinit var service: CertificationService

    @PostMapping("/add/{userId}")
    fun addCertification(@Valid @RequestBody cerDTO: CertificationDTO, @PathVariable userId: UUID): ResponseEntity<CertificationDTO> {
        return ResponseEntity.ok(service.addCer(cerDTO, userId))
    }


    @PutMapping("/update/{id}")
    fun updateCertification(@PathVariable id: UUID, @RequestBody cerUpdateDTO: CertificationUpdateDTO): ResponseEntity<CertificationDTO> {
           return ResponseEntity.ok(service.updateCer(cerUpdateDTO, id))
    }


    @GetMapping("/{id}")
    fun getCertification(@PathVariable id: UUID): ResponseEntity<CertificationDTO> {
        return ResponseEntity.ok(service.getCertification(id))
    }

    @DeleteMapping("delete/{id}")
    fun deleteCertification(@PathVariable id: UUID): ResponseEntity<String> {
        service.deleteCer(id)
        return ResponseEntity.ok("Done")
    }

}