package com.reem.univer_pro.career.entityDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CareerDTO {

    private UUID careerId;

    private UUID userId;

    @JsonProperty("company_name")
    private String companyName;

    @JsonProperty("position")
    private String position;

    @JsonProperty("duration")
    private String duration;

    @JsonProperty("entered_year")
    private String enteredYear;


    public CareerDTO build(Career career) {
        this.careerId = career.getCareerId();
        this.userId = career.getUserId();
        this.companyName = career.getCompanyName();
        this.position = career.getPosition();
        this.duration = career.getDuration();
        this.enteredYear = career.getEnteredYear();

        return this;
    }
}
