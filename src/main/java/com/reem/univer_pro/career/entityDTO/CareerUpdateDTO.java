package com.reem.univer_pro.career.entityDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CareerUpdateDTO {

    @JsonProperty("company_name")
    private String companyName;

    @JsonProperty("position")
    private String position;

    @JsonProperty("duration")
    private String duration;

    @JsonProperty("entered_year")
    private String enteredYear;


}
