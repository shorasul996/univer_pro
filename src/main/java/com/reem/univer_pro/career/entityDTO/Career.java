package com.reem.univer_pro.career.entityDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "career")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Career {

    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "career_id")
    private UUID careerId;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "company_name")
    @Size(max = 100, message = "company_name can not be more than 100 characters.")
    private String companyName;

    @Column(name = "position")
    @Size(max = 100, message = "position can not be more than 100 characters.")
    private String position;

    @Column(name = "duration")
    @Size(max = 100, message = "duration can not be more than 100 characters.")
    private String duration;

    @Column(name = "entered_year")
    @Size(max = 100, message = "entered_year can not be more than 100 characters.")
    private String enteredYear;



    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_created")
    private Date timeCreated = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time_Modified")
    private Date timeModified = new Date();

}
