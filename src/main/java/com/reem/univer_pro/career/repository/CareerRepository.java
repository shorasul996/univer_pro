package com.reem.univer_pro.career.repository;

import com.reem.univer_pro.career.entityDTO.Career;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CareerRepository extends JpaRepository<Career, UUID> {

    Career findByCareerId(UUID id);
}
