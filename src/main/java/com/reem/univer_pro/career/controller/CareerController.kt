package com.reem.univer_pro.career.controller

import com.reem.univer_pro.career.entityDTO.CareerDTO
import com.reem.univer_pro.career.entityDTO.CareerUpdateDTO
import com.reem.univer_pro.career.service.CareerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/career")
class CareerController {

    @Autowired
    lateinit var service: CareerService

    @PostMapping("/add/{userId}")
    fun addCareer(@Valid @RequestBody careerDTO: CareerDTO, @PathVariable userId: UUID): ResponseEntity<CareerDTO> {
        return ResponseEntity.ok(service.addCareer(careerDTO, userId))
    }


    @PutMapping("/update/{id}")
    fun updateCareer(@PathVariable id: UUID, @RequestBody careerUpdateDTO: CareerUpdateDTO): ResponseEntity<CareerDTO> {
           return ResponseEntity.ok(service.updateCareer(careerUpdateDTO, id))
    }


    @GetMapping("/{id}")
    fun getCareer(@PathVariable id: UUID): ResponseEntity<CareerDTO> {
        return ResponseEntity.ok(service.getCareer(id))
    }

    @DeleteMapping("delete/{id}")
    fun deleteCareer(@PathVariable id: UUID): ResponseEntity<String> {
        service.deleteCareer(id)
        return ResponseEntity.ok("Done")
    }

}