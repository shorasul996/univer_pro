package com.reem.univer_pro.career.service;

import com.reem.univer_pro.career.entityDTO.Career;
import com.reem.univer_pro.career.entityDTO.CareerDTO;
import com.reem.univer_pro.career.entityDTO.CareerUpdateDTO;
import com.reem.univer_pro.career.repository.CareerRepository;
import com.reem.univer_pro.excaption.*;
import com.reem.univer_pro.user.entityDTO.User;
import com.reem.univer_pro.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class CareerService implements iCareerService {

    private final CareerRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public CareerService(CareerRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }


    @Override
    public CareerDTO addCareer(CareerDTO careerDTO, UUID userId) {

        User user = userRepository.findUserByUserId(userId);

        if (user != null) {

            Date now = new Date();
            List<Career> careerList = user.getCareerList();
            Career career = new Career();

            career.setUserId(userId);
            career.setCompanyName(careerDTO.getCompanyName());
            career.setPosition(careerDTO.getPosition());
            career.setEnteredYear(careerDTO.getEnteredYear());
            career.setDuration(careerDTO.getDuration());
            career.setTimeCreated(now);
            career.setTimeModified(now);
            repository.save(career);

            careerList.add(career);
            user.setCareerList(careerList);

            careerDTO.setCareerId(career.getCareerId());
            careerDTO.setUserId(career.getUserId());
            return careerDTO;
        }

        return null;
    }

    @Override
    public CareerDTO updateCareer(CareerUpdateDTO careerUpdateDTO, UUID id) {

        Career career = repository.findByCareerId(id);
        if (Objects.isNull(career)) {
            throw new EntityNotFoundException(Career.class, id);
        }
        career.setCompanyName(careerUpdateDTO.getCompanyName());
        career.setPosition(careerUpdateDTO.getPosition());
        career.setEnteredYear(careerUpdateDTO.getEnteredYear());
        career.setDuration(careerUpdateDTO.getDuration());
        career.setTimeModified(new Date());

        repository.save(career);
        return new CareerDTO().build(career);
    }

    @Override
    public CareerDTO getCareer(UUID id) {

        Career career = repository.findByCareerId(id);
        if (Objects.isNull(career)) {
            throw new EntityNotFoundException(Career.class, id);
        }
        return new CareerDTO().build(career);
    }

    @Override
    public void deleteCareer(UUID id) {
        repository.deleteById(id);
    }


}
