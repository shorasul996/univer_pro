package com.reem.univer_pro.career.service;

import com.reem.univer_pro.career.entityDTO.CareerDTO;
import com.reem.univer_pro.career.entityDTO.CareerUpdateDTO;

import java.util.UUID;


public interface iCareerService {

    CareerDTO addCareer(CareerDTO careerDTO, UUID userId);

    CareerDTO updateCareer(CareerUpdateDTO careerUpdateDTO, UUID id);

    CareerDTO getCareer(UUID id);

    void deleteCareer(UUID id);
}
